package com.example.demo.BookAuthorAPIv2;

import java.util.ArrayList;

public class Book {
    public String name ;
    public ArrayList<Author> author ;
    public double price ;
    public int qty ;
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Author> getAuthor() {
        return author;
    }

    public void setAuthor(ArrayList<Author> author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Book() {
    }

    public Book(String name, ArrayList<Author> author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book(String name, ArrayList<Author> author, double price, int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }

    public String getAuthorNames() {
        String lstname = "";
        for (int i = 0; i < author.size(); i++) {
            lstname = author.get(i).getName();
        }
        return lstname;
    }


    @Override
    public String toString() {
        return "Book [name= " + name +" [author= { " + getAuthor().toString() + "}" +  ", price=" + price + ", qty=" + qty + "]";
    }

}
