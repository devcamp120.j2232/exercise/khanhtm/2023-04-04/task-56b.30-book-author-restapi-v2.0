package com.example.demo.BookAuthorAPIv2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/book2")
    public ArrayList<Book> getListBook(){
        ArrayList<Book> books = new ArrayList<>();

        Author author1 = new Author("Jis00" , 'F' ,"bl@yahoo.com");
        Author author2 = new Author("Lisa" , 'F' ,"yg@yahoo.com");
        Author author3 = new Author("Jennie" , 'F' ,"blyg@yahoo.com");
        Author author4 = new Author("Rosie" , 'F' ,"blyg123@yahoo.com");
        Author author5 = new Author("BlackPink" , 'F' ,"hihi345@yahoo.com");
        Author author6 = new Author("YG" , 'M' ,"ygggggggg@yahoo.com");

    
        ArrayList<Author> authorList1 = new ArrayList<Author>();
        ArrayList<Author> authorList2 = new ArrayList<Author>();
        ArrayList<Author> authorList3 = new ArrayList<Author>();

        authorList1.add(author1);
        authorList1.add(author2);
        authorList2.add(author3);
        authorList2.add(author4);
        authorList3.add(author5);
        authorList3.add(author6);


        Book book1 = new Book("YGGG" , authorList1 ,34.00,131);
        Book book2 = new Book("CSSS" , authorList2,122.00,92999);
        Book book3 = new Book("FEEE" , authorList3,442.00,70888);  

  
        books.add(book1);
        books.add(book2);
        books.add(book3);

        return books ;
    }
}
